import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {
  
  numero1!:any;
  numero2!:any;
  total!:number;
  constructor() {
   }

   sumar(){
     this.total=parseInt(this.numero1)+parseInt(this.numero2);
   }

   ngOnInit(): void {
  }

}
